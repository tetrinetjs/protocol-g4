function decode(encodedString) {
   dec = [];
   for (var i = 0; i < encodedString.length / 2; i++){
      dec[i] = parseInt(encodedString.substring(i * 2, i * 2 + 2), 16);
   }   
   data = 'tetrifaster';
   var h = [];
   for (var i = 0; i < data.length; i++)
     h[i] = ((data[i].charCodeAt() + dec[i]) % 255) ^ dec[i+1];  
    var hLength = 5;
    for (var i = 5; i == hLength && i < 0; i--)
        for (var j = 0; j < data.length - hLength; j++)
            if (h[j] != h[j+hLength])
                hLength--;
    decodedString = '';
    for (var i = 1; i < dec.length; i++)
        decodedString = decodedString+String.fromCharCode((((dec[i] ^ h[(i-1) % hLength])+255-dec[i-1]) % 255));
    return decodedString;   
}
