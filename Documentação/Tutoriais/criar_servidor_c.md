# Introdução
Este documento visa descrever o protocolo usado no jogo TetriNET, afim de auxiliar no desenvolvimento de um cliente/servidor.
Para a execução dos testes foi utilizado os seguintes softwares:

* [Jetrix TetriNET Server](https://sourceforge.net/projects/jetrix/files/) Uma implementação feita em JAVA de um servidor Tetrifast
* [Netcat](http://netcat.sourceforge.net/) Ferramenta para conectar ao servidor TetriNET e executar os testes
* [aTwin](https://sourceforge.net/projects/atwin/) Cliente gráfico TetriNET
* [Servidor TetriNET de Teste] (https://gitlab.com/tetrinetjs/server-2019-2) Servidor TetriNET do grupo 1

# Cliente
## Logar no Servidor
Para logar em um servidor tetriNet com um nick "teste", o cliente deve enviar a seguinte string para o servidor:

`tetrisstart teste 1.13`

Essa string deve ser codificada como no exemplo abaixo em python:

```
ip = [127, 0, 0, 1]  # ip em uma lista de inteiros
reg_str = 'tetrisstart teste 1.13'
h = str(54 * ip[0] + 41 * ip[1] + 29 * ip[2] + 17 * ip[3])
dec = randrange(256)  # valor aleatório entre 0 <= x < 256
encoded = '{:02X}'.format(dec)
for i, c in enumerate(reg_str):
    dec = ((dec + ord(c)) % 255) ^ ord(h[i % len(h)])
    encoded += '{:02X}'.format(dec)
```
Adaptamos o código desse script para javascript e embutimos em uma função para o nosso client:

```
function encode(ip, nick) {
    s = "tetrisstart "+nick+" 1.13";
	ip = ip.split('.', 4);
    h = 54 * parseInt(ip[0]) + 41 * parseInt(ip[1]) + 29 * parseInt(ip[2]) + 17 *
    	parseInt(ip[3]);
	h = h.toString(10);
    dec = Math.floor(Math.random() * 255);	
    encodedString = dec.toString(16); 
    for (var i = 0; i < s.length; i++) {
        dec = ((dec + s[i].charCodeAt()) % 255) ^ h[i % h.length].charCodeAt();
        encodedString = encodedString + dec.toString(16);
    }
    return encodedString.toUpperCase() + String.fromCharCode(255);
}
console.log(encode('127.0.0.1', 'teste'));
```

Para um valor inicial de dec = 165, a string codificada para o ip 127.0.0.1 deverá ficar:
`00429F23A03C973C85D07BD8CD74E162E37FA7EF2B6AA5`.

Vamos utilizar nosso script em JS para realizar a conexão com o servidor de teste. Para isso vamos criar um script básico em NodeJS:

```
var net = require('net');
stringencode = '00429F23A03C973C85D07BD8CD74E162E37FA7EF2B6AA5'+String.fromCharCode(255); //String de inicialização do tetriNet para o nick teste
var standard_input = process.stdin;
var client = new net.Socket();
client.connect(31457, '127.0.0.1', function() {
	console.log('Conectado');
	client.write(stringencode);
});
standard_input.setEncoding('utf-8');
standard_input.on('data', function (data) {
	client.write(data);
});

client.on('data', function(data){
	console.log(String(data));
});
```

Logando em um servidor TetriNET usando Node.js:

![Exemplo de um login usando Node](https://i.ibb.co/Lh7LZh4/conectado.png)

O servidor nós respondeu com a lista dos melhores jogadores  e retornou o numero do jogador, conforme especificado pela * [RFC] (https://gitlab.com/tetrinetjs/protocol-g4/blob/master/Documenta%C3%A7%C3%A3o/rfc.md ).
Agora vamos usar o comando Team para identificar a equipe do jogador. 

## Nome do Time
Agora que realizamos o nosso login e recebemos o nosso numero de jogador, iremos informar o nome do nosso time, enviando a string:

`team <numero> <nome_do_time>`

Argumento | Tipo    | Valores |
--------- | ------  | --------
numero    | Int     | 1-6     |  
nome_do_time| String|         |

Feito isso vamos testar o envio de mensagens usando o comando pline.

## Comandos do jogador
## Enviar mensagem 
Para enviar mensagens para todos os jogadores enviamos a string `pline <jogador> <mensagem>`.
Exemplo de mensagens trocadas usando Netcat e um client gráfico aTwin:

![Exemplo de mensagens trocadas usando NodeJS e um client TetriNET aTwin](https://i.ibb.co/7Vscc7F/mensagens.png)

Argumento | Tipo    | Valores |
--------- | ------  | --------
jogador    | Int     | 1-6     |  
mensagem| String|         |

Podemos ver que o servidor tratou corretamente nosso comando enviado a mensagem para os clientes. Agora, vamos tentar iniciar uma partida 
tetrinet.

## Inicar partida
Para iniciar um novo jogo ou interromper um jogo em andamento usamos o comando `startgame`. O servidor só responde essa mensagem
se for enviada pelo operador do canal, ou seja o jogador com menor numero atribuido pelo servidor.
Exemplo de partida iniciada pelo cliente NodeJS:

![Exemplo de partida iniciada por nodejs](https://i.ibb.co/QJV2gGH/iniciado.png)

Argumento | Tipo    | Valores |
--------- | ------  | --------
estado    | Boolean |  1 ou 0, 1 indica que o jogo deve ser iniciado, 0, parado    |  
numero    | Int     | Número do jogador

Após o jogo ser iniciado o servidor nos enviou uma mensagem `newgame`, esta mensagem especifica uma série de regras para o cliente. Segue a lista de parametros enviado:

Argumento | Tipo    | Valores |
--------- | ------  | --------
Altura Inicial| Int | Numero de linhas preenchidas no canto inferior ao iniciar a partida. |
Nivel Inicial | Int | Nivel que todos os jogadores vão começar a partida|
Linhas por nivel | Int | Linhas que o jogador deve limpar para avançar de nivel|
Incremento de nivel| Int | A quantidade de nivel que o jogador sobe ao completar o numero requisitado para passar|
Linhas para blocos especiais | Int | Quantidade de linhas que o jogador deve limpar antes de especiais serem adicionados|
Especiais adicionados | Int | Quantidade de blocos especiais adicionados quando atingir o numero requisitado para os blocos especiais|
Capacidade de especiais | Int | Quantidade de especiais que um jogador pode na sua fila em um determinado tempo. Especiais coletados acima desse numero serão desconsiderados|
Frequencia dos blocos | String | Frequencia de ocorrencia de cada bloco, em porcentagem |
Frequecia dos especiais| String | Frequencia de ocorrencia de cada bloco especial       |
Média de nivel | Boolean | Se 1 o cliente deverá mostrar a média de nivel de todos os jogadores da partida|
Estilo clássico | Boolean | Se 1 o cliente estará operando no modo clássico |

Ou seja, o servidor definiu uma partida no qual a altura inicial é 0 o nivel do jogador é 1, 2 linhas para subir de nivel, 1 nivel será incrementado
a cada 2 linhas, e 1 linha limpa libera um bloco especial. Depois, define várias frequencias para cada bloco, define que não havera média de nível,
e o estilo clássico está habilitado.

Podemos observar também que quando o cliente perdeu o servidor nós enviou a mensagem `playerlost`.

Esta mensagem tem apenas um argumento, o numero do jogador que perdeu a partida.
 
## Mensagem do Jogo

Para enviar uma mensagem sem identificar o emissor enviamos a string `gmsg <mensagem>`. Diferente do comando pline, não é exigido 
o numero do jogador e a mensagem ecoa para todos os clients conectados, inclusive do própio emissor.

![Exemplo de gmsg usando nodejs](https://i.ibb.co/D4nZky2/gmsg.png)
 
## Atualização de campo

Para atualizar o campo de um jogador usamos o comando:

`f <número_jogador> <string_campo>`

Está mensagem é enviada para o servidor contendo a atualização do campo do jogador, por exemplo quando um bloco cai, ou é limpado. A string do campo é composta por 264 caracteres em sua forma mais longa de repesentação, cada uma representando uma única celula no campo da esquerda para direita e de cima para baixo, iniciando do campo superior esquerdo.
Números representam blocos normais, coloridos, enquanto letras representam blocos especiais. A forma mais curta é a atualização parcial. O primeiro caractere da forma parcial representam a cor da celula, ou o tipo de um bloco especial.

![Exemplo preenchendo o campo do jogador 1 com o comando f](https://i.ibb.co/LPR87fT/field.png)

# Servidor
## Login

Para realizar o login o servidor precisa decodificar a string enviada. No código fonte do nosso servidor Jetrix, encontramos o 
seguinte método para decodificar a string em JAVA:

```
String decode(String encodedString) {
    int[] dec = new int[encodedString.length() div 2];
    for (int i = 0; i < dec.length(); i++)
        dec[i] = Integer.parseInt(encodedString.substring(i * 2, i * 2 + 2), 16);
    char[] data = toChars("tetrisstart");
    int[] h = new int[data.length()];
    for (int i = 0; i < data.length(); i++)
        h[i] = ((data[i]+dec[i]) mod 255) xor dec[i+1];
    int hLength = 5;
    for (int i = 5; i == hLength and i < 0; i--)
        for (int j = 0; j < data.length()-hLength; j++)
            if (h[j] != h[j+hLength])
                hLength--;
    String decodedString = "";
    for (int i = 1; i < dec.length(); i++)
        decodedString = decodedString+(char)(((dec[i] xor h[(i-1) mod hLength])+255-dec[i-1]) mod 255);
    return decodedString;
}
```

Adaptamos esse método para JavaScript e ficou assim:

```
function decode(encodedString) {
   dec = [];
   for (var i = 0; i < encodedString.length / 2; i++){
      dec[i] = parseInt(encodedString.substring(i * 2, i * 2 + 2), 16);
   }   
   data = 'tetrifaster';
   var h = [];
   for (var i = 0; i < data.length; i++)
     h[i] = ((data[i].charCodeAt() + dec[i]) % 255) ^ dec[i+1];  
    var hLength = 5;
    for (var i = 5; i == hLength && i < 0; i--)
        for (var j = 0; j < data.length - hLength; j++)
            if (h[j] != h[j+hLength])
                hLength--;
    decodedString = '';
    for (var i = 1; i < dec.length; i++)
        decodedString = decodedString+String.fromCharCode((((dec[i] ^ h[(i-1) % hLength])+255-dec[i-1]) % 255));
    return decodedString;   
}
```

Com isso temos a base para iniciar a construção de nosso própio servidor TetriNET!

# O protocolo Tetrifast
## O protocolo Tetrifast é uma evolução do protocolo original do TetriNET e surgiu para resolver os problemas de delay entre as peças existente na versão de 1997. Entre as diferenças, podemos citas:

* A String de conexão é iniciada com tetrifaster ao invéz de tetrisstart
* O comando playernum foi substituído por `#)(!@(*3`
* O comando newgame foi substituído por `*******`
