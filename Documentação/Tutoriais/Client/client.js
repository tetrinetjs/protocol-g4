function encode(ip, nick) {
    s = "tetrisstart "+nick+" 1.13";
	ip = ip.split('.', 4);
    h = 54 * parseInt(ip[0]) + 41 * parseInt(ip[1]) + 29 * parseInt(ip[2]) + 17 *
    	parseInt(ip[3]);
	h = h.toString(10);
    dec = Math.floor(Math.random() * 255);	
    encodedString = dec.toString(16); 
    for (var i = 0; i < s.length; i++) {
        dec = ((dec + s[i].charCodeAt()) % 255) ^ h[i % h.length].charCodeAt();
        encodedString = encodedString + dec.toString(16);
    }
    return encodedString.toUpperCase() + String.fromCharCode(255); 
}
var os = require('os');
var ifaces = os.networkInterfaces();
var net = require('net');
if(process.argv.length < 5){
	console.log('Use: node client.js ip porta nick');
	process.exit(0);
}	
stringencode = encode(process.argv[2], process.argv[4]);

var client = new net.Socket();
client.connect(process.argv[3], process.argv[2], function() {
	console.log('Conectado');
	client.write(stringencode);
});
client.on('data', function(data){
	console.log(String(data));
});

