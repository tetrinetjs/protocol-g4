    Internet Engineering Task Force (IETF)                          Bruna   Carneiro 
    Request for Comments: 20192                                     Victor  Vidal
    Category: Informational                                         Odilon  Rodrigues
	                                                                Gabriel Aleixo
                                                                    Lucas  Naves
                                                                    Universidade Federal de Goiás
                                                                    September 2019


 
## Abstract

Este documento especifica o protocolo TetriNet e suas variações, suscitando debates e sugestões de 
melhorias com a comunidade.

## Situação deste Memorando

Este documento é parte do projeto da disciplina de sistemas distribuídos do 2° semestre de 2019 do curso de Sistemas da Informação da 
Universidade Federal de Goiás, tutorada pelo professor Marcelo Akira Inuzuka. A finalidade deste documento é educacional e didática, estando
aberto a comunidade da internet para ser distribuída, respeitando os limites da [licença FDL](https://www.gnu.org/licenses/fdl-1.3.html). 


## Índice

* 1. Introdução
   * 1.1. Convenções Utilizadas neste Documento
   * 1.2. Terminologia
   * 1.3. Visão Geral
* 2. Requisiçao de acesso
  * 2.1. Fluxo de requisição
  * 2.2. Criptografia
* 3. Mensagens  
  * 3.1. Visão Geral
*  4.0. Detalhes das Mensagens
    * 4.1. Requisiçao de autenticação
    * 4.2. Iniciar partida
	* 4.3. Comandos da partida

## 1.  Introdução

O protocolo TetriNet define os meios de comunicação para uma aplicação cliente/servidor do jogo homônimo. 

### 1.1.  Convenções Utilizadas neste Documento

As palavras-chave “REQUER”, “DEVE”, “NÃO DEVE”, “PODERIA”, “NÃO PODERIA” e “PODE” neste documento devem ser interpretadas como
descritas em “Key words for use in RFCs to Indicate Requirement Levels” [RFC2119].

### 1.2.  Terminologia

**Canal** - Este é um canal de jogo que aceita clientes que jogam e que não jogam
(expectadores). As mensagens passam por um conjunto de filtros que podem transformar, excluir ou adicionar mensagens para personalizar o comportamento do canal. Após o processamento, a mensagem do jogo é enviada de volta aos clientes.

**Operador do canal**  - Jogador com maior poder em um canal tetrinet. Ele pode iniciar, parar, pausar e continuar jogos, e pode ter maior
poder a depender das configurações do servidor tetrinet.
    

### 1.3.   Visão Geral

O protocolo TetriNet define a comunicação entre um cliente/servidor TetriNet. Sendo um protocolo da camada de aplicação, ele utiliza
tcp ou udp para envio dos pacotes. De modo geral, TetriNet é um jogo multiplayer baseado no clássico Tetris com até 6 jogadores em rede.
Por padrão, servidores TetriNet utilizam a porta 3457 tcp/udp.

## 2. Requisiçao de acesso
Esta seção descreve o funcionamento da autenticação em um servidor TetriNet.

### 2.1. Fluxo de requisição

O acesso ao servidor Tetrinet se dá através de uma string que é criptografada e convertida para hexadecimal,
utilizando um algoritmo de chave simétrica.
Após o envio da string, o servidor deve responder com o número do jogador.

    +--------+                                             +---------------+
    | Cliente|--(A)- Requisição de acesso(criptografado) ->|   Servidor    |
    |        |<-(B)-- Número do jogador ---                |               |
    +--------+                                             +---------------+

   Figura 1: Fluxo de autenticação básica 

[protocolo] [nick] [versão]

Protocolo: Nome do protocolo podendo ser tetrisstart para a versão clássica do servidor TetriNet, ou, tetrifaster para a versão mais
recente.

Nick: Apelido utilizado pelo jogador.

Versão: Versão do protocolo.

## 2.2. Criptografia

O algoritmo de criptografia utiliza o ip do servidor e um número decimal pseudo aleatório entre 0 e 255.
Com esses valores, o algoritmo realiza alguns cálculos para retornar uma string hexadecimal ilegível. Para isso, o algoritmo pega o primeiro byte do endereço ip do servidor, multiplica por 54 e soma o resultado com a multiplicação do segundo byte por 41. Continua com a soma da multiplicaçao de 29 com o terceiro byte, e termina com a soma da multiplicação do último byte por 17.

Resultado = 54 * 127 + 41 * 0 + 29 * 0 + 17 * 1
		
		Figura 2: Exemplo da equação utilizando o ip do servidor como 127.0.0.1. O resultado = 6875
		
A chave da criptografia é composta pelo resultado dessa equação e um número decimal entre 0 e 255. O algoritmo soma o valor 
pseudo aleatório com o código ASCII em decimal de cada caractere da string sendo enviada e com o resultado realiza uma operação OU EXCLUSIVO byte a byte com o código ASCII de cada caractere resultado da equação.

    RESULTADO = (NUMERO_ALEATORIO + CODIGO_ASCII_CADA_CARACTERE_DA_STRING) OU EXCLUSIVO (CODIGO_ASCII_CADA_CARACTERE_DO_RESULTADO_DO_IP) 
	
	   Figura 3: Exemplo da obtenção da cifra criptografada

## 3. Mensagens
Esta seção descreve as mensagens trocadas entre o cliente e o servidor TetriNet.

## 3.1. Visão Geral
Cada mensagem consiste em um comando e parâmetros. O comando e os parâmetros são separados entre eles pelo caractere de
espaço ASCII (0x20).
As mensagens devem terminar com um par CR-LF (Carriage Return - Line Feed).
Uma mensagem poderá ou não ser respondida pelo servidor.

## 4.0. Detalhes das mensagens

Esta seção irá descrever cada mensagem reconhecida pelo servidores e clientes TetriNet. Todos os comandos descritos devem ser implementados
por qualquer cliente/servidor deste protocolo.

Quando há algum erro na requisição, o servidor não deverá retornar nenhuma mensagem.


## 4.1. Requisiçao de autenticação

Os comandos descritos aqui são usados para registrar uma nova conexão com um servidor TetriNet tanto, para o servidor, quanto para o cliente.
A ordem de mensagens de uma autenticação, é a seguinte:

    1. tetrisstart
	2. winlist
	3. playernum
    4. playerjoin
	5. team
	
## 4.1.1. Tetrisstart

	Comando: testrisstart
	Parâmetros: <nick> <versao>

Este comando é usado pelo cliente para iniciar uma nova conexão com servidor tetrinet. Ele deve ser codificado conforme descrito na sessão 2.2.

## 4.1.2. Winlist

	Comando: winlist
	Parâmetros: <tipo><nome>;<pontuacao> <tipo><nome>
	
Esta mensagem é enviada pelo servidor para o cliente e traz o ranking de jogadores. O tipo é um caractere que identifica
se a pontuação é de um jogador ou de um time. Para jogador o caractere é P e para time é T.
	
## 4.1.3. Playernum

	Comando: playernum
	Parâmetros: <número_jogador>
	
Esta mensagem é enviada pelo servidor após o cliente iniciar a conexão e a qualquer momento que o servidor necessitar para reorganizar a ordem de jogadores. O número do jogador é gerado pelo servidor, devendo estar entre 1 e 6.
O jogador com o menor número é o operador do canal.

## 4.1.4. Playerjoin

	Comando: playerjoin
	Parâmetros: <número_jogador> <nick>

Esta mensagem é enviada pelo servidor para todos os clientes conectados, informando que um novo jogador acaba de se conectar.

## 4.1.5. Team
	
	Comando: team
	Parâmetros: <número_jogador> <nome_time>

Este comando é enviado pelo cliente para o servidor e serve para identificar a equipe do jogador.

## 4.2. Iniciar partida

Os comandos descritos aqui são enviados para iniciar uma nova partida TetriNet tando pelo servidor quanto pelo cliente.
A ordem de mensagens para iniciar uma partida, é a seguinte:

    1. startgame
	2. newgame

## 4.2.1. Startgame

	Comando: startgame
	Parâmetros: <estado> <número_jogador>
	
Comando enviado pelo cliente para o servidor para iniciar um novo jogo ou parar uma partida em andamento. O parâmetro estado é um booleano com dois valores possíveis:
1 ou 0, 1 inicia a partida e 0 para. Apenas o operador do canal pode utilizar este comando.

## 4.2.2. Newgame
 
	Comando: newgame
	Parâmetros: <altura_inicial> <nivel_inicial> <linhas_por_nivel><incremento_por_nivel><linhas_para_blocos_especiais><especiais_adicionados><capacidade_de_especiais><frequencia_dos_blocos><frequencia_dos_especiais><média_de_nivel><Estilo_classico>
	
Mensagem enviada pelo servidor após iniciar partida. Esta mensagem especifica uma série de regras para o cliente. Os seguintes parametros são enviados:

Altura Inicial: Especifica o numero de linhas preenchidas no inicio do jogo

Nível inicial:  Nível que os jogadores começarão na partida. O nível não pode exceder 100

Linhas por Nível: Quantidade de linhas que jogador deve limpar para avançar de nível

Incremento de Nível: A quantidade nível que é incrementado caso o jogador alcance o número de linhas que deve limpar para avançar

Linhas para blocos especiais: Quantidade de linhas que o jogador deve limpar para antes das especiais serem adicionadas

Especiais adicionados: Quatidade  de blocos especiais quando a quantidade de linhas para blocos especiais for atingido

Capacidade de especiais: Quantidade de especiais que um jogador pode na sua fila em um determinado tempo. Especiais coletados acima desse numero serão desconsiderados

Frequencia dos blocos: Frequencia de ocorrencia de cada bloco, em porcentagem 

Frequecia dos especiais: Frequencia de ocorrencia de cada bloco especial

Média de nivel: Se 1 o cliente deverá mostrar a média de nivel de todos os jogadores da partida

Estilo clássico: Se 1 o cliente estará operando no modo clássico

## 4.3. Comandos da partida
Os comandos descritos aqui são enviados durante uma partida de TetriNet tando pelo servidor quanto pelo cliente.

## 4.3.1. Gmsg

	Comando: gmsg
	Parâmetros: <mensagem>

Comando exclusivo do operador do canal, envia mensagem anônima durante a partida.

## 4.3.2. Playerlost

	Comando: playerlost
	Parâmetros: <número_jogador>

Mensagem enviada pelo servidor quando um jogador perde a partida. 

## 4.3.3. Atualização dos campos

	Comando: f
	Parâmetros: <número_jogador> <string_campo>

Mensagem enviada para o servidor contendo a atualização do campo do jogador, por exemplo quando um bloco cai, ou é limpado. A string do campo é composta por 264 caracteres em sua forma mais longa de repesentação, cada uma representando uma única celula no campo da esquerda para direita e de cima para baixo, iniciando do campo superior esquerdo.
Números representam blocos normais, coloridos, enquanto letras representam blocos especiais. A forma mais curta é a atualização parcial. O primeiro caractere da forma parcial representam a cor da celula, ou o tipo de um bloco especial.

## 4.3.4. Ingame

	Comando: ingame
	
Mensagem enviada do servidor para o cliente quando ele se conecta. Indica que existe uma partida em andamento.

